import { useState } from "react";
import "./UserForm.css";

function UserForm() {

  const [formDetiles, setFormDetiles] = useState({
      stooge: "Larry",
      Employed: false
  });
  const [isChecked, setIsChecked] = useState(false);
  const [stooge, setStooge] = useState("Larry");
  const [sauces, setSauces] = useState([]);
  const [formIsCanged, setFormIsCanged] = useState(false);


  const handleChange = e => {
    setFormIsCanged(true);
    if(e.target.name === "Employed") {
      setIsChecked(!isChecked);
      e.target.value = !isChecked;
    }
    if(e.target.name === "stooge") {
      setStooge(e.target.value);
    }
    if(e.target.name === "sauces"){
      var saucesArr = [...sauces];
      if (e.target.checked) {
        saucesArr = [...sauces, e.target.value];
      } else {
        saucesArr.splice(sauces.indexOf(e.target.value), 1);
      }
      setSauces(saucesArr);
      e.target.value = saucesArr;
    }
    const nextFormState = {
      ...formDetiles,
      [e.target.name]: e.target.value,
    };
    setFormDetiles(nextFormState);
  }

  const handleSubmit = e => {
    e.preventDefault();
    alert(JSON.stringify(formDetiles, null, 2));
  };

  const handleReset = () => {
    setFormIsCanged(false);
  }


  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <div>
          <label for="first-name">First Name</label>
          <input pattern = "[a-zA-Z]+" className="right-side" id="first-name" name="firstName" placeholder="First Name" value={formDetiles.firstName} onChange={handleChange}></input>
        </div>
        <div>
          <label for="last-name">Last Name</label>
          <input pattern = "[a-zA-Z]+" className="right-side" id="last-name" name="lastName" placeholder="Last Name" value={formDetiles.lastName} onChange={handleChange}></input>
        </div>
        <div>
          <label for="age">Age</label>
          <input className="right-side" id="age" type="number" name="age" placeholder="Age" value={formDetiles.age} onChange={handleChange}></input>
        </div>
        <div>
          <label for="Employed">Employed</label>
          <input className="right-side" id="employed" type="checkbox" name="Employed" checked={isChecked} value={formDetiles.Employed} onChange={handleChange}></input>
        </div>
        <div>
          <label for="favourite-color">Favourite Color</label>
          <select className="right-side" name="favouriteColor" onChange={handleChange}>
            <option value="#ffffff"></option>
            <option value="#ffgfff">Green</option>
            <option value="#fbffff">Blue</option>
            <option value="#ffffff">Red</option>
            <option value="#ffffff">Black</option>
          </select>

        </div>
        <div className="sauces-input">
          <label for="sauces">Sauces</label>
          <div className="right-side" id="sauces">
            <div>
              <input id="ketchup" type="checkbox" value="Ketchup" name="sauces" onChange={handleChange}></input>
              <label for="ketchup">Ketchup</label>
            </div>
            <div>
              <input id="Mustard" type="checkbox" value="Mustard" name="sauces" onChange={handleChange}></input>
              <label for="Mustard">Mustard</label>
            </div>
            <div>
              <input id="Mayonnaise" type="checkbox" value="Mayonnaise" name="sauces" onChange={handleChange}></input>
              <label for="Mayonnaise">Mayonnaise</label>
            </div>
            <div>
              <input id="Guacamole" type="checkbox" value="Guacamole" name="sauces" onChange={handleChange}></input>
              <label for="Guacamole">Guacamole</label>
            </div>
          </div>
        </div>
        <div className="storage-input">
          <label for="best-storage">Best Storage</label>
          <div className="right-side" name="stooge">
            <div>
              <input type="radio" id="Larry" value="Larry" checked={stooge === "Larry"} onChange={handleChange} name="stooge"></input>
              <label for="Larry">Larry</label>
            </div>
            <div>
              <input type="radio" id="Moe" value="Moe" checked={stooge === "Moe"} onChange={handleChange} name="stooge"></input>
              <label for="Moe">Moe</label>
            </div>
            <div>
              <input type="radio" id="Curly" value="Curly" checked={stooge === "Curly"} onChange={handleChange} name="stooge"></input>
              <label for="Curly">Curly</label>
            </div>
          </div>
        </div>
        <div>
          <label for="notes">Notes</label>
          <textarea
            className="right-side"
            id="notes"
            maxLength='100'
            type="textarea"
            placeholder="Notes"
            name="notes"
            value={formDetiles.notes}
            onChange={handleChange}
          ></textarea>
        </div>
        <div className="buttons">
          <input className="submit" type="submit" value="Submit" disabled={!formIsCanged}></input>
          <input className="reset" type="reset" value="Reset" disabled={!formIsCanged} onClick={handleReset}></input>
        </div>
      </form>
    </div>
  );
}

export default UserForm;
